<?php
function getConfig() {
    return [
    'DB_NAME' => 'NAME',
    'DB_PASSWORD' => 'PASSWORD',
    'DB_SERVER' => 'SERVER',
    'DB_USER' => 'USER',
    'XML_FILE' => '/tmp/import.xml'
    ];
}
