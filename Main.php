#!/usr/bin/env php
<?php
require_once(__DIR__ . '/SapModuleXml.php');
require_once(__DIR__ . '/CopyToDb.php');
require_once(__DIR__ . '/config.php');
require_once(__DIR__ . '/simpleXmlStreamer.php');

print "Read and parse XML.\n";
$xmlAssoc = new Xml2Assoc();
$config = getConfig();

//$full=$xmlAssoc->parseFile('/scratch/share/system/projekte/2022_module_xml/20220204_CM_DATEN_EXPORT.lint.XML');
//$fullData = $xmlAssoc->parseFile('/scratch/share/system/projekte/2022_module_xml/module_tab.xml');
//$fullData = $xmlAssoc->parseFile('/scratch/share/system/projekte/2022_module_xml/module_event_tab.xml');

$fullData = $xmlAssoc->parseFile($config['XML_FILE']);
$copyToDb = new CopyToDb();
$copyToDb->process($fullData);

//$copyToDb->truncate("master");
//$streamer = new SimpleXmlStreamer($config['XML_FILE'], 65526, "MASTERDATA");
//$streamer->parse();
//
//// Relation
//$copyToDb->truncate("relation");
//$relationStreamer = new SimpleXmlStreamer($config['XML_FILE'], 65526);
//$relationStreamer->parse();

