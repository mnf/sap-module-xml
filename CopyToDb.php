<?php

require_once(__DIR__ . '/Constants.php');
require_once(__DIR__ . '/Database.php');


class CopyToDb {

    private $db = null;

    public function __construct() {
        $this->db = new Database(DB_INDEX_DEFAULT);

    }

    /**
     * @param $module
     * @return array
     */
    private function cleanModule($module) {
        foreach ($module as $key => $value) {
            $new[$key] = $value[0] ?? null;
        }
        return ($new);
    }

    /**
     * Parse and write all MODULES
     *
     * @param $moduleAll
     * @return void
     * @throws Exception
     */
    private function module($moduleAll) {

        print "Copy Module to DB.\n";
        $this->db->sql("TRUNCATE module_tab");

        foreach ($moduleAll as $module) {

            $module = $this->cleanModule($module);
            $this->db->sql("INSERT INTO module_tab
             
                           (`otype`,`objid`,`langu`,`short`,`stext`,`sm_category`,`buchbar`,`valid_begda`,
                           `valid_endda`,`cancel_begda`,`cancel_endda`,`cpmax`,`vvz_url`,`vb_tab`,`diszip_tab`)
                            
                           VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", ROW_REGULAR,

                [$module['OTYPE'] ?? '', $module['OBJID'] ?? '', $module['LANGU'] ?? '', $module['SHORT'] ?? '',
                    $module['STEXT'] ?? '', $module['SM_CATEGORY'] ?? '', $module['BUCHBAR'] ?? '',
                    $module['VALID_BEGDA'] ?? '', $module['VALID_ENDDA'] ?? '', $module['CANCEL_BEGDA'] ?? '',
                    $module['CANCEL_ENDDA'] ?? '', $module['CPMAX'] ?? '', $module['VVZ_URL'] ?? '', 'vb_tab', 'diszip_tab']);
        }
    }

    /**
     * @param $eventAll
     * @return void
     * @throws Exception
     */
    private function termin($objid, array $terminEvent) {

//        print "Copy Termin to DB.\n";
        foreach ($terminEvent as $termin) {
            $raum ='';
            if (isset($termin['RAUM_TAB'])) {
                if (is_array($termin['RAUM_TAB']['RAUM']??'')) {
                    $raum = $termin['RAUM_TAB']['RAUM'][0];
                }
            }
            $this->db->sql("INSERT INTO termin_tab (`event_objid`,`evdat`,`beguz`,`enduz`,`raum`) VALUES (?,?,?,?,?)"
                , ROW_REGULAR, [$objid, $termin['EVDAT'][0] ?? '', $termin['BEGUZ'][0] ?? '', $termin['ENDUZ'][0] ?? '', $raum]);
        }
    }

    

    /**
     * @param $eventAll
     * @return void
     * @throws Exception
     */
    private function event($eventAll) {

        print "Copy Event/Termin/Raum to DB.\n";
        $this->db->sql("TRUNCATE event_tab");
        $this->db->sql("TRUNCATE termin_tab");

        foreach ($eventAll as $event) {
            $this->db->sql("INSERT INTO event_tab (`objid`,`short`,`stext`,`vvz_url`) VALUES (?,?,?,?)"
                , ROW_REGULAR, [$event['OBJID'][0] ?? '', $event['SHORT'][0] ?? '', $event['STEXT'][0] ?? '', $event['VVZ_URL'][0] ?? '']);

            if (isset($event['TERMIN_TAB'])) {
                $this->termin($event['OBJID'][0] , $event['TERMIN_TAB']);
            }
        }
    }

    public function eventSingle($event) {
        $this->db->sql("INSERT INTO event_tab (`objid`,`short`,`stext`,`vvz_url`) VALUES (?,?,?,?)"
                , ROW_REGULAR, [$event->OBJID ?? '', $event->SHORT ?? '', $event->STEXT ?? '', $event->VVZ_URL ?? '']);
    }

    /**
     * @param $personAll
     * @return void
     * @throws Exception
     */
    private function person($personAll) {

        print "Copy Person to DB." . PHP_EOL;
        $this->db->sql("TRUNCATE person_tab");

        foreach ($personAll as $person) {
            $this->db->sql("INSERT INTO person_tab (`pernr`, `vname`, `name1`, `itim_short`, `itim_mail`, `email`, `gesch`, `pub_titel`, `titel`, `persk`, `sprsl`, `funktionscode`, `fachrunicode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
                , ROW_REGULAR, [$person['PERNR'][0] ?? '', $person['VNAME'][0] ?? '', $person['NAME1'][0] ?? '', $person['ITIM_SHORT'][0] ?? '', $person['ITIM_MAIL'][0] ?? '', $person['EMAIL'][0] ?? '', $person['GESCH'][0] ?? '', $person['PUB_TITEL'][0] ?? '', $person['TITEL'][0] ?? '', $person['PERSK'][0] ?? '', $person['SPRSL'][0] ?? '', $person['FUNKTIONSCODE'][0] ?? '', $person['FACHRUNICODE'][0] ?? '']);
        }
    }

    public function personSingle($person) {
        $this->db->sql("INSERT INTO person_tab (`pernr`, `vname`, `name1`, `itim_short`, `itim_mail`, `email`, `gesch`, `pub_titel`, `titel`, `persk`, `sprsl`, `funktionscode`, `fachrunicode`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            , ROW_REGULAR, [$person->PERNR ?? '', $person->VNAME ?? '', $person->NAME1 ?? '', $person->ITIM_SHORT ?? '', $person->ITIM_MAIL ?? '', $person->EMAIL ?? '', $person->GESCH ?? 0, $person->PUB_TITEL ?? '', $person->TITEL ?? '', $person->PERSK ?? '', $person->SPRSL ?? '', $person->FUNKTIONSCODE ?? '', $person->FACHRUNICODE ?? '']);
    }

    private function eventType($eventTypeAll) {
        print "Copy Event Types to DB." . PHP_EOL;
        $this->db->sql("TRUNCATE eventtype_tab");

        foreach ($eventTypeAll as $eventType) {
            $this->db->sql("INSERT INTO eventtype_tab (`otype`, `objid`, `langu`, `short`, `stext`, `d_category`, `method`) VALUES (?,?,?,?,?,?,?)"
                , ROW_REGULAR, [$eventType['OTYPE'][0] ?? '', $eventType['OBJID'][0] ?? '',$eventType['LANGU'][0] ?? '', $eventType['SHORT'][0] ?? '', $eventType['STEXT'][0] ?? '',$eventType['D_CATEGORY'][0] ?? '', $eventType['METHOD'][0] ?? '']);
        }
    }

    public function eventTypeSingle($eventType) {
        $this->db->sql("INSERT INTO eventtype_tab (`otype`, `objid`, `langu`, `short`, `stext`, `d_category`, `method`) VALUES (?,?,?,?,?,?,?)"
                , ROW_REGULAR, [$eventType->OTYPE ?? '', $eventType->OBJID ?? '',$eventType['LANGU'][0] ?? '', $eventType->SHORT ?? '', $eventType->STEXT ?? '',$eventType['D_CATEGORY'][0] ?? '', $eventType->METHOD ?? '']);
    }

    private function relation($relationAll) {
        print "Copy Relations to DB." . PHP_EOL;
        $this->db->sql("TRUNCATE relation_tab");

        foreach ($relationAll as $relation) {
            $this->db->sql("INSERT INTO relation_tab (`otype`, `objid`, `relat`, `sclas`, `sobid`, `aw_based`) VALUES (?,?,?,?,?,?)"
                , ROW_REGULAR, [$relation['OTYPE'][0] ?? '', $relation['OBJID'][0] ?? '', $relation['RELAT'][0] ?? '', $relation['SCLAS'][0] ?? '', $relation['SOBID'][0] ?? '', $relation['AW_BASED'][0] ?? NULL]);
        }
    }

    public function relationSingle($relation) {
        $this->db->sql("INSERT INTO relation_tab (`otype`, `objid`, `relat`, `sclas`, `sobid`, `aw_based`) VALUES (?,?,?,?,?,?)"
                , ROW_REGULAR, [$relation->OTYPE ?? '', $relation->OBJID ?? '', $relation->RELAT ?? '', $relation->SCLAS ?? '', $relation->SOBID ?? '', $relation->AW_BASED ?? NULL]);
    }

    private function booking($bookingAll) {
        print "Copy Booking to DB." . PHP_EOL;
        $this->db->sql("TRUNCATE booking_tab");
        #$this->db->sql("INSERT INTO room_tab (`otype`, `objid`, `langu`, `short`, `stext`, `kapz1`, `ort01`, `build`, `room1`"
        #    , ROW_REGULAR, [$room->OTYPE ?? '', $room['OBJID'][0], $room['LANGU'][0] ?? '', $room['SHORT'][0] ?? '', $room['STEXT'][0] ?? '', $room['KAPZ1'][0] ?? 0, $room['ORT01'][0] ?? '', $room['BUILD'][0] ?? '', $room['ROOM1'][0] ?? '']);
        
        foreach($bookingAll as $booking) {
            $this->db->sql("INSERT INTO booking_tab (`adatanr`, `smstatus`, `cpattemp`, `norm_val`, `bookdate`, `booktime`, `packnumber`, `alt_scaleid`, `id`, `bookreason`, `lockflag`, `cobok`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                , ROW_REGULAR, [$booking['ADATANR'][0] ?? '', $booking['SMSTATUS'][0] ?? '', $booking['CPATTEMP'][0] ?? '', $booking['NORM_VAL'][0] ?? '', $booking['BOOKDATE'][0] ?? '1901-01-01', $booking['BOOKTIME'][0] ?? '00:00:00', $booking['PACKNUMBER'][0] ?? '', $booking['ALT_SCALEID'][0] ?? '', $booking['ID'][0] ?? '', $booking['BOOKREASON'][0] ?? '', $booking['LOCKFLAG'][0] ?? '', $booking['COBOK'][0] ?? '']);
        }
    }

    public function bookingSingle($booking) {
        $this->db->sql("INSERT INTO booking_tab (`adatanr`, `smstatus`, `cpattemp`, `norm_val`, `bookdate`, `booktime`, `packnumber`, `alt_scaleid`, `id`, `bookreason`, `lockflag`, `cobok`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            , ROW_REGULAR, [$booking->ADATANR ?? '', $booking->SMSTATUS ?? '', $booking->CPATTEMP ?? '', $booking->NORM_VAL ?? '', $booking->BOOKDATE ?? '1901-01-01', $booking->BOOKTIME ?? '00:00:00', $booking->PACKNUMBER ?? '', $booking->ALT_SCALEID ?? '', $booking->ID ?? '', $booking->BOOKREASON ?? '', $booking->LOCKFLAG ?? '', $booking->COBOK ?? '']);
    }


    public function roomSingle($room) {
        $this->db->sql("INSERT INTO room_tab (`otype`, `objid`, `langu`, `short`, `stext`, `kapz1`, `ort01`, `build`, `room1`"
            , ROW_REGULAR, [$room->OTYPE ?? '', $room->OBJID, $room->LANGU ?? '', $room->SHORT ?? '', $room->STEXT ?? '', $room->KAPZ1 ?? 0, $room->ORT01 ?? '', $room->BUILD ?? '', $room->ROOM1 ?? '']);
    }

    public function truncate($target) {
        if ($target == "master") {
            $this->db->sql("TRUNCATE person_tab");
            $this->db->sql("TRUNCATE booking_tab");
            #$this->db->sql("TRUNCATE room_tab");
            $this->db->sql("TRUNCATE eventtype_tab");
        } else {
            $this->db->sql("TRUNCATE relation_tab");
        }
        
    }




    /**
     * Write Data
     *
     * @param array $fullData
     * @return void
     * @throws Exception
     */
    public function process(array $fullData) {

        //$this->module($fullData['DATA'][0]['MASTERDATA']['MODULE_TAB']);
        //$this->event($fullData['DATA'][0]['MASTERDATA']['EVENT_TAB']);
        //$this->person($fullData['DATA'][0]['MASTERDATA']['PERSON_TAB']);
        $this->eventType($fullData['DATA'][0]['MASTERDATA']['EVENTTYPE_TAB']);
        $this->booking($fullData['DATA'][0]['MASTERDATA']['BOOKING_TAB']);
        //$this->relation($fullData['DATA'][0]['RELATION_TAB']);

    }
}