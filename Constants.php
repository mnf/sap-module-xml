<?php

const DB_INDEX_DEFAULT = 0;
const SYSTEM_DB_INIT = 'init';
const SYSTEM_DB_USER = 'DB_USER';
const SYSTEM_DB_SERVER = 'DB_SERVER';
const SYSTEM_DB_PASSWORD = 'DB_PASSWORD';
const SYSTEM_DB_NAME = 'DB_NAME';

const ERROR_UNKNOWN_MODE = 1032;
const ERROR_UNKNOWN_SQL_LOG_MODE = 1051;
const ERROR_PLAY_SQL_MULTIQUERY = 1087;
const ERROR_PLAY_SQL_FILE = 1080;


// DB Errors
const ERROR_DB_OPEN = 2000;
const ERROR_DB_EXECUTE = 2001;
const ERROR_DB_PREPARE = 2002;
const ERROR_DB_BIND = 2003;
const ERROR_DB_QUERY = 2004;
const ERROR_DB_GENERIC_CHECK = 2005;
const ERROR_DB_TOO_FEW_ROWS = 2006;
const ERROR_DB_TOO_MANY_ROWS = 2007;
const ERROR_DB_COUNT_DO_NOT_MATCH = 2008;
const ERROR_DB_CLOSE_MYSQLI_RESULT = 2009;
const ERROR_DB_CLOSE_MYSQLI_STMT = 2010;
const ERROR_DB_UNKNOWN_COLUMN = 2011;
const ERROR_DB_UNKNOWN_COMMAND = 2012;

const ERROR_DB_COLUMN_NOT_FOUND_IN_TABLE = 2014;
const ERROR_DB_SET_CHARSET = 2015;
const ERROR_DB_MULTI_QUERY_FAILED = 2016;

// https://lib2.colostate.edu/wildlife/atoz.php?letter=ALL
// JSON encoded messages thrown through an exception:
const ERROR_MESSAGE_TO_USER = 'toUser'; // always shown to the user.
const ERROR_MESSAGE_TO_DEVELOPER = 'support'; // Message to help the developer to understand the problem.
const ERROR_MESSAGE_TO_DEVELOPER_SANITIZE = 'support_sanitize'; // Typically 'true' or missing. If 'false' then content of 'support' won't be html encoded.
const ERROR_MESSAGE_OS = 'os'; // Error message from the OS - like 'file not found' or specific SQL problem
const ERROR_MESSAGE_HTTP_STATUS = 'httpStatus'; // HTTP Status Code to report

// dbQuerySimple
const ROW_REGULAR = "regular";
const ROW_IMPLODE_ALL = "implode_all";
const ROW_EXPECT_0 = "expect_0";
const ROW_EXPECT_1 = "expect_1";
const ROW_EXPECT_0_1 = "expect_0_1";
const ROW_EXPECT_GE_1 = "expect_ge_1";
const ROW_KEYS = "keys";


const QUERY_TYPE_SELECT = 'type: select,show,describe,explain';
const QUERY_TYPE_INSERT = 'type: insert';
const QUERY_TYPE_UPDATE = 'type: update,replace,delete';
const QUERY_TYPE_CONTROL = 'type: set';
const QUERY_TYPE_FAILED = 'type: query failed';

// DATABASE
const DB_NUM_ROWS = 'numRows';
const DB_AFFECTED_ROWS = 'affectedRows';
const DB_INSERT_ID = 'insertId';

const COLUMN_ID = 'id';
const COLUMN_FIELD = 'Field';
const COLUMN_CREATED = 'created';
const COLUMN_MODIFIED = 'modified';


// SQL logging Modes
const SQL_LOG_MODE_ALL = 'all';
const SQL_LOG_MODE_MODIFY = 'modify';
const SQL_LOG_MODE_NONE = 'none';
const SQL_LOG_MODE_ERROR = 'error';

// DataImport
const IMPORT_MODE_APPEND = 'append';
const IMPORT_MODE_REPLACE = 'replace';
const IMPORT_REGION_DEFAULT = '1';
const IMPORT_PREFIX = 'Import_';

