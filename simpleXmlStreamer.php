<?php
require_once(__DIR__ . '/xmlStreamer.php');
//require_once(__DIR__ . '/config.php');
require_once(__DIR__ . '/CopyToDb.php');

class SimpleXmlStreamer extends \Prewk\XmlStreamer {
    protected $pdo;
    protected $sql = array();
    protected $values = array();

    /**
     * Called after the constructor completed class setup
     */
    public function init()
    {
        //$this->config = getConfig();
        $this->copyToDb = new CopyToDb();
        //$this->pdo = new PDO('mysql:host=' . $this->config['DB_SERVER'] .';dbname=' . $this->config['DB_NAME'], $this->config['DB_USER'], $this->config['DB_PASSWORD']);
    }

    public function processNode($xmlString, $elementName, $nodeIndex) {
        switch($elementName) {
            case "PERSON_TAB":
                $xml = simplexml_load_string($xmlString);
                $this->copyToDb->personSingle($xml);
                break;
            case "RELATION_TAB":
                $xml = simplexml_load_string($xmlString);
                $this->copyToDb->relationSingle($xml);
                break;
            case "BOOKING_TAB":
                $xml = simplexml_load_string($xmlString);
                $this->copyToDb->bookingSingle($xml);
                break;
            case "EVENTTYPE_TAB":
                $xml = simplexml_load_string($xmlString);
                $this->copyToDb->eventTypeSingle($xml);
                break;
        }
        
        // Do something with your SimpleXML object
        //print_r($xml);
        //print($elementName);

        /*
        $this->sql[] = '(?,?,?)';
        $this->values[] = (string)$xml->name;
        $this->values[] = (string)$xml->email;
        $this->values[] = (string)$xml->phone;
        */

        return true;
    }

    /**
     * Called after a file chunk was processed (16KB by default, see constructor)
     */
    /*public function chunkCompleted()
    {
        if($this->sql===array()) {
            return;
        }
        $command = $this->pdo->prepare('INSERT INTO mytable VALUES '.implode(',',$this->sql));
        $command->execute($this->values);

        $this->sql = $this->values = array();
    }*/
}

